#!/bin/bash

echo - main: `date +"%Y%m%d%H%M"` >> README.adoc

# use a provided HTTP with token url to allow the push
git status
git config user.email bob-thebuilder@yourcompany.com
git config user.name Bob TheBuilder
git checkout master
echo token: $TOKEN_URL
git remote add token $TOKEN_URL || git remote set-url --add --push token $TOKEN_URL
git commit -am "[ci skip] release: update project version"
git push -u token master